//#href:index.html


"use strict";


if (!Object.values) {
	Object.values = function (obj) {
		var result = [];
		for (var i in obj) {
			if (obj.hasOwnProperty(i)) {
				result.push(obj[i]);
			}
		}
		return result;
	};
}

// 64 cuadritos
// 64 * 10 * 10

function error(str) {
	var err = new Error(str);
	error.last = err;
	throw err;
}

function andCall(f) {
	f();
	return f;
}

function strFiller(str, length, filler) {
	filler = filler || "_";
	while (str.length < length) {
		str += filler;
	}
	return str;
}

function strfy(a) {
	return JSON.stringify(a);
}

function generateID() {
	if (!generateID._last) {
		generateID._last = 0;
	}
	return ++generateID._last;
}

function inRange(a, b, c) {
	if (!/^\d+$/.test(a + b + c)) {
		error("InRange need 3 Numbers");
	}
	return a >= b && a <= c;
}


function display() { // Debugging

	var red = function (a) { return '<span style="color:red">' + a + '</span>'; };

	var _map = map.data.subarray();
	var show = '';
	var step = map.cols;

	for (var i = _map.length - 1; i >= 0; i -= step) {
		for (var j = step - 1; j >= 0; j--) {
			var id = _map[i - j];
			if (id === 1) {
				id = red('1');
			}
			if (id === 2) {
				id = red('2');
			}
			show += id;
		}
		show += "\r\n";
	}
	return show;
}

function log(a) {
	console.log.apply(console, arguments);
}



var map = {};
map.cols = 32;
map.rows = 32;
map.data = new Int16Array(map.cols * map.rows);
map.calcPoint = function (val) {
	var y = val / map.cols | 0;
	var x = val - y * map.cols;
	return {
		x: x,
		y: y
	};
};




var sprites = {};
var _animations = [];

sprites._byIndex = new Array(1024);
sprites._byName = {};
sprites._counter = 0;


function addListeners(inst) {
	inst.on("move", function (event, data) {
		_animations.push({action: "move", inst: inst, data: data});
	});
}

/* TOFIX: There is a bug because it evaluates X first than Y!!! */


var xxx = 0;
setInterval(function () {
	_animations.sort(function (a, b){
		var _a = map.calcPoint(a.inst._pos + a.data.deltaX + a.data.deltaY * map.cols);
		var _b = map.calcPoint(b.inst._pos + a.data.deltaX + a.data.deltaY * map.cols);
		var	fx = _a.x - _b.x;
		var	fy = _a.y - _b.y;
		var go = 0;

		if (fx < 0 && a.data.deltaX === 1 || fx > 0 && a.data.deltaX === -1) {
			go++;
		}

		if (fy < 0  && a.data.deltaY === 1 || fy > 0  && a.data.deltaY === -1) {
			go++;
		}

		// > 0 hace que b sea menor index que a
		return go;
	});

	for (var index = 0; index < _animations.length; index) {

		var ani = _animations.splice(index, 1)[0];

		var inst = ani.inst;
		var delay = ani.delay || 100;


		if (inst.type === TYPE.UNIQUE || inst.type === TYPE.CHARACTER) {
			if (ani.action === "move") {


				var point = map.calcPoint(inst._pos);

				// deltaX y Y no puede ser mayor a 1
				var deltaX = ani.data.deltaX;
				var deltaY = ani.data.deltaY;

				var insideX = inRange(point.x + deltaX, 0, map.cols - 1);
				var insideY = inRange(point.y + deltaY, 0, map.rows - 1);

				var tmpX = deltaX;
				var tmpY = deltaY * map.cols;

				var accessX;
				var accessY;

				var testBoth = sprites._byIndex[map.data[inst._pos + tmpX + tmpY]];
				var accessBoth = !testBoth || (!inst.is(testBoth.type.aside) && !testBoth.is(inst.type.aside));


				if ( !accessBoth || !insideX || !insideY ) {

					var testX = sprites._byIndex[map.data[inst._pos + tmpX]];
					accessX = !testX || (!inst.is(testX.type.aside) && !testX.is(inst.type.aside));

					var testY = sprites._byIndex[map.data[inst._pos + tmpY]];
					accessY = !testY || (!inst.is(testY.type.aside) && !testY.is(inst.type.aside));

				} else {

					accessX = true;
					accessY = true;

				}

				map.data[inst._pos] = null;

				if (insideX && accessX) inst._pos += tmpX;
				if (insideY && accessY) inst._pos += tmpY;
				
				map.data[inst._pos] = inst.ID;


				$("body").html("<pre>" + display() + "</pre>");

			}
		}
	}
}, 25);



sprites.create = function (obj) {

	obj = $.extend({}, obj);
	
	var requiredProps = {
		name: "string",
		description: "string",
		type: "object",
		locations: "array"
	};

	$.each(requiredProps, function (prop, val) {
		var t = typeof obj[prop];
		if (t !== val && (val !== "array" || !Array.isArray(obj[prop]))) {
			error("Sprite's property '" + prop + "' must be " + val + " not '" + t + "'");
		}
	});

	if (sprites._byName[obj.name]) {
		error("Sprite with name '" + obj.name + "' is already defined");
	}

	var Inst = function () {};
	var proto = $.extend({}, TYPE._BASE.prototype, obj.type.prototype);

	for (var prop in proto) {
		if (proto.hasOwnProperty(prop)) {
			Inst.prototype[prop] = proto[prop];
		}
	}

	var inst = new Inst();
	$.extend(true, inst, obj.type.props);
	$.extend(inst, obj);

	inst.index = ++sprites._counter;
	sprites._byIndex[inst.index] = inst;
	sprites._byName[inst.name] = inst;
	
	inst.on = function (a, f) {
		$(inst).on(a, f);
	};

	inst.trigger = function (e, data) {
		$(inst).trigger(e, data);
	};

	addListeners(inst);

	inst.ini.call(inst, inst);

	$.each(inst.locations, function (i, pos) {
		var j = pos.y * map.cols + pos.x;
		inst.ID = generateID();
		map.data[j] = inst.ID;
		inst._pos = j;
	});

	console.log(strfy(obj),strfy(inst));
	return inst;

};


sprites.get = function (needle) {
	var type = typeof needle;
	var errMsg = "Sprites couldn't be found, needle: " + needle;

	if (type === "number") {
		return sprites._byIndex[needle] || error(errMsg);
	} else if (type === "string") {
		return sprites._byName[needle] || error(errMsg);
	} else {
		error("sprites.get only accept strings or numbers, '" + type + "' given instead.");
	}
};

sprites.remove = function (needle) {
	var s = sprites.get(needle);
	delete sprites._byIndex[s.index];
	delete sprites._byName[s.name];
};

var KEY = {
	UP: 38,
	DOWN: 40,
	LEFT: 37,
	RIGHT: 39
};

var TYPE = {
	_BASE: {
		prototype: {
			is: function (keys) {
				var type = this.type;
				var result = false;
				if (keys === type.aside) {
					return true;
				}
				// log(type);
				$.each(keys, function (i, k) {
					if (type.type === TYPE[k]) {
						result = true;
					}
				});
				return result;
			}
		}
	},
	FLOOR: {
		aside: ["UNWALKABLE"]
	},
	UNWALKABLE: {
		aside: ["FLOOR", "ITEM"]
	},
	ITEM: {
		aside: ["UNWALKABLE"]
	},
	CHARACTER: {
		aside: ["ITEM", "UNWALKABLE"],
		props: {
			speed: 4,

			go: {
				up: false,
				down: false,
				left: false,
				right: false
			},

			interval: null
		},

		prototype: {
			moveHandler: function () {
				// log("triggered handler for", this.name);
				var self = this;
				clearInterval(self.interval);
				var first = true;
				self.interval = setInterval(andCall(function () {
					var _go = $.extend({}, self.go);
					if (first) {
						var max = Math.max(_go.up, _go.down, _go.right, _go.left);
						$.each(_go, function (i, ele) {
							if (ele !== max) {
								_go[i] = false;
							}
						});
					}
					first = false;
					if (_go.up || _go.down || _go.right || _go.left)
						self.trigger("move", {
							deltaY: !_go.up && !_go.down ? 0 : _go.up - _go.down > 0 ? 1 : -1,
							deltaX: !_go.right && !_go.left ? 0 : _go.right - _go.left > 0 ? 1 : -1
						});
				}), 400 / self.speed);
			}
		}
	},
	UNIQUE: {
		deny: []
	}
};


/*sprites.create({
	name: "empty",
	type: TYPE.FLOOR,
	locations: [],
	pos: [] str
});
*/

window.arg = [];

var opts = {
	name: "player",
	description: "The character for the player",
	type: TYPE.CHARACTER,
	locations: [{x: 0, y: 0}],
	ini: function (self) {

		$(window).on("blur", function (e) {
			self.go.up = false;
			self.go.right = false;
			self.go.left = false;
			self.go.down = false;
			self.moveHandler();
		});

		$(window).on("keydown keyup", function (e) {
			var addDirection = ("keydown" === e.type);
			var sides = Object.keys(self.go);
			var changed = false;
			

			sides.forEach(function (side) {
				if (e.which === KEY[side.toUpperCase()]) {
					e.preventDefault();
					var p = addDirection ? +new Date() : false;
					if (Boolean(p) !== Boolean(self.go[side])) {
						changed = true;
						self.go[side] = p;
					}
				}
			});
			if (addDirection && changed) {
				self.moveHandler();
			}
		});
	}
};





sprites.create($.extend({}, opts, {name: 'player2', locations:[{x:1, y:2}]}));

sprites.create(opts);



// cfdgdfg dfg  fdg dfg fdg dfdfg dfgd f dfd fg dfg dfg d
// dgdf gdf gdf gd fgd fgfd g dfg dfgdfgdfgdf dfgdfgdfg
// dsf sdf sdfsd ds fsd f sdf sdfs df
// dsf xcvxcvcxv
// 3124442124
// 6966820



// Lunes 6:00 pm