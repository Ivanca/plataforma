"use strict";

function generateID() {
	if (!generateID._last) generateID._last = 0;
	return ++generateID._last;
}

function randomInt(min, max) {
	return min + Math.floor(Math.random() * (max - min + 1)) | 0;
}

function randomStr() {
	return String(Math.random()).match(/\d{2}/g).map(String.fromCharCode).join("");
}

var row = 32;
var col = 32;

var map = {
	data: new Int16Array(row * col)
};

var sprites = {};
var TYPES = {};
sprites._byIndex = [];

for (var i = 20; i >= 0; i--) {
	TYPES[randomStr()] = i;
}

for (var i = 1000; i >= 0; i--) {
	sprites._byIndex[i] = [];
	var r = randomInt(1, 3);
	for (var j = 0; j < r; j++) {
		var interactions = [];
		if (randomInt(0, 1))
			interactions[randomInt(0, 1000)] = {walk : function () {}};
		 
		sprites._byIndex[i].push({
			id: generateID(), // The Unique ID
			size: {width: 2, height: 4}, // The size in the grid
			type: randomInt(0, 20),
			locations: [{x: randomInt(0, row), y: randomInt(0, col)}], // If isloading an existing map
			interactions: interactions // Interactions by ID with other objets
		});
	}
}

/* TYPES AND SPRITES AVAILABLE FROM NOW ON*/





