/*globals game*/

"use strict";

var sister = game.newGirl("sister");
var player = game.getPlayer(1);

sister.on("talk", function (dialog) {
	dialog.say("What do you want?");

	dialog.options({
		"I need some money!": function () {
			if (!player.has("money")) {
				player.addItem("money");
				dialog.exit("Ok take 5!");
			} else {
				dialog.exit("I already gave you 5!");
			}
		},
		"Nothing": function () {
			dialog.exit();
		}
	});

	if (player.has("pajama")) {
		dialog.options({"I found your pajama" : function () {
			sister.happy = true;
			player.loseItem("pajama");
			dialog.exit("you rock! Thanks!");
		}});
	}
});

sister.on("kill", function () {
	game.lost("WTF?! You killed your sister!");
});

