
sister = game.newGirl "sister"
player = game.getPlayer 1

sister.on "talk", (dialog) ->
	dialog.say "What do you want?"

	dialog.options
		"I need some money!" : ->
			if !player.has "money"
				player.addItem "money"
				dialog.exit "Ok take 5!"
			else
				dialog.exit "I already gave you 5!"
		"Nothing" : -> dialog.exit "Ok!"

	dialog.options if player.has "pajama"
		"I found your pajama!" : ->
			sister.happy = yes
			player.loseItem "pajama"
			dialog.exit "You rock! Thanks!"

sister.on "kill", -> game.lost "WTF?! You killed your sister!"