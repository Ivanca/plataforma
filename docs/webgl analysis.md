WebGL Analysis (Iv�n C.)
-------------------------

Uno de los principales objetivos del proyecto es crear una plataforma que pueda ser usada para que la gente programe de una forma social y que pueda crear juegos interesantes y bonitos. Para esto los gr�ficos tienen que poder ser desarrollados por cualquier dise�ador que pueda crear gr�ficos vectoriales de algun tipo. 

La cantidad de personas que pueden crear gr�ficos 2D es exponencialmente mayor a la cantidad de personas que pueden crear gr�ficos 3d. Si alguien quisiera contratar un dise�ador 3D estos �ltimos son muy escasos y cobran mucho.

Considero que ya tenemos bastantes cosas nuevas que aprender que nunca hab�amos hecho antes; entre ellas est�n: un parser o pseudo-parser Javascript que puede subrayar las lineas de c�digo que se est�n ejecutando en cualquier momento. Un editor de niveles de videojuego que sea amigable y que permita a novatos que no saben Javascript crear niveles y ver el c�digo siendo creado en vivo y en directo. Igualmente creo que habr�n muchos conceptos y tecnolog�as necesarias para crear exitosamente esta plataforma que todav�a no hemos previsto.

Igualmente el tiempo necesario para crear gr�ficos 3D interesantes es mucho mayor que el necesario para crear gr�ficos 2D interesantes; y teniendo en cuenta que este es un proyecto sin presupuesto que depende del tiempo y ganas de los colaboradores es un poco arriesgado usar webgl si queremos que sea exitoso.

La cantidad de dispositivos que soporta WebGL es mucho menor y los que los soportan tienen una cantidad significativa de bugs y problemas de rendimiento.

Por las razones mencionadas ofrezco las siguientes 3 opciones respecto al desarrollo WebGL en esta plataforma:

- Sistema de pelea en 3D: que sea posible usar un sistema de pelea en 3D -con fallback en canvas si el dispositivo no lo soporta- usando dos mu�ecos est�ticos de los personajes y que se puedan seleccionar ataques tipo RPG.

- Lo mas viable seria un mix de los 2. Usar WEBGL para peque�as interacciones sobre canvas -con fallback en canvas si el dispositivo no lo soporta-; por ejemplo una pelea de espadas; estas podr�an estar renderizadas en webgl siendo ambas en 3D usando un renderizado caricaturesco (vease borderlands). Tambi�n para objetos que se lanzan (piedras, flechas, etc) o solo para ayudar a dar inmersi�n en ciertas interacciones (veanse puertas en Resident Evil)

- 2 Renders: uno para canvas aunque la primera versi�n de WebGL tiene que ser elementos sin texturas; de a un color diferente cada uno con animaciones muy simples.