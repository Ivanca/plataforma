Otros tipos de uso
-----------------------

- Permitir la facil creacion de material didactico y ludico para niños y jovenes. Por ejemplo un juego donde uno viaje por todo el mundo peliando y para entrar a un pais le pidan saber el nombre del pais

- Incentivar a los jovenes a crear sus propias misiones programando (aprendiendo los conceptos de programacion en el camino)

- Herramienta para crear juegos interactivos donde el propio juego le enseña a codificar (no creando niveles, si no completandolos)

- Para crear mapas/tours virtuales; por ejemplo un parque de diversiones que quiera mostrar interactivamente las atracciones y la ubicacion de estas.