###################################
# - Introduction (non-playable) - #
###################################

game.loadScene "House"
yeisson = game.people.get "player"
sir = game.people.get "richole"


sir.say "Yeisson, come here"
yeisson.moveTo x: 30, y: 10
sir.say "Our daughter lost both of our dog yesterday in the forest" 
yeisson.say "You could say... she let the dogs out!"
yeisson.animation( "sunglasses" )
sir.say "I would beat you right now but my wife is going to kill me if I don't get those goddamn dogs back"
sir.say "Help me out in this one and I will give you $30"
yeisson.say "Make it $150 and we are in bussiness!"
sir.say "$35"
yeisson.say "Deal! But you will let me use your pool this sunday"
sir.say "Ok, but you need to take a bath before that, you stink!"
yeisson.say "This is how her tacos smell! Maybe you forgot because you haven't had some fun in a while Mr. Rabbiole"
sir.say "IS RICHOLE! Now shut up and get in the car."


####################
# - In the woods - #
####################

# starting point
game.loadScene "Forest"
truck = game.objects.get "truck"

yeison.change x: 30, y: 11 
sir.say "Now search for those dumb bitches"
yeison.say "That's what I do every day Sir!"
sir.say "..."
truck.moveTo deltaX: -50, deltaY: 0

cave = game.get "cave"
bark = game.sounds.get "bark"


# cave
cave.on "closer"
	bark.volumen += 1

cave.on "farther"
	bark.volumen -= 1

cave.on "action", ->	
	yeisson.say "Hell no! The dog may be there but is way too dark!"


# hose
hose = game.objects.get "hose"
hose.description = "Just an old hose"

hose.on "action", ->
	yeison.addItem hose


# lighter
lighter = game.objects.get "lighter"
lighter.description = "Just an empty lighter"

lighter.on "action", ->
	yeison.think this.description
	yeison.addItem lighter


# emptyCar
emptyCar = game.objects.get "emptyCar"
emptyCar.description = "An abandoned car"

emptyCar.on "action", ->
	yeison.think this.description

emptyCar.on "action:lighter", ->
	yeison.think "Even if I wanted to set the car on fire... I could not"


# Solving 1
emptyCar.on "action:hose", ->
	yeison.think "There may gasoline left in this thing I could suck out with this"
	yeison.loseItem "hose"
	yeison.think "Ok, there is some gasoline left; but I need something to store it"
	emptyCar.scene("hose")
	emptyCar.off()
	emptyCar.on "action:lighter", ->
		fullLighter = game.objects.get("fullLighter")
		fullLighter.description = "A full lighter"
		yeison.think "Yes, this could work"
		yeison.addItem fullLighter
		yeison.think "Excellent! The lighter is full now"

# Solving 2
cave.on "action:fullLighter", ->
	yeison.say "There you are!"
	game.win("End of the demo! To continue buy the full version, lol joking! We are super lazy so there is no full version..")

